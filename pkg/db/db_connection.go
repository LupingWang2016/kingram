/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package db

import (
	"context"
	api_pb "delphin/api/v1alpha1/manager"
	"delphin/pkg/controllers/consts"
	"google.golang.org/grpc"
)

type delphinDBManagerClientAndConn struct {
	Conn                   *grpc.ClientConn
	DelphinDBManagerClient api_pb.ManagerClient
}

// GetDBManagerAddr returns address of Delphin DB Manager
func GetDBManagerAddr() string {
	dbManagerNS := consts.DefaultDelphinDBManagerServiceNamespace
	dbManagerIP := consts.DefaultDelphinDBManagerServiceIP
	dbManagerPort := consts.DefaultDelphinDBManagerServicePort

	if len(dbManagerNS) != 0 {
		return dbManagerIP + ":" + dbManagerPort //"30333" //"." + dbManagerNS +
	}

	return dbManagerIP + ":" + dbManagerPort
}

func getDelphinDBManagerClientAndConn() (*delphinDBManagerClientAndConn, error) {
	addr := GetDBManagerAddr()
	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		return nil, err
	}
	kcc := &delphinDBManagerClientAndConn{
		Conn:                   conn,
		DelphinDBManagerClient: api_pb.NewManagerClient(conn),
	}
	return kcc, nil
}

func closeDelphinDBManagerConnection(kcc *delphinDBManagerClientAndConn) {
	kcc.Conn.Close()
}

func GetObservationLog(request *api_pb.GetObservationLogRequest) (*api_pb.GetObservationLogReply, error) {
	ctx := context.Background()
	kcc, err := getDelphinDBManagerClientAndConn()
	if err != nil {
		return nil, err
	}
	defer closeDelphinDBManagerConnection(kcc)
	kc := kcc.DelphinDBManagerClient
	return kc.GetObservationLog(ctx, request)
}

func DeleteObservationLog(request *api_pb.DeleteObservationLogRequest) (*api_pb.DeleteObservationLogReply, error) {
	ctx := context.Background()
	kcc, err := getDelphinDBManagerClientAndConn()
	if err != nil {
		return nil, err
	}
	defer closeDelphinDBManagerConnection(kcc)
	kc := kcc.DelphinDBManagerClient
	return kc.DeleteObservationLog(ctx, request)
}
