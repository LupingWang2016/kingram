package db

import (
	"database/sql"
	"fmt"
	"time"

	v1alpha1 "delphin/api/v1alpha1/manager"
	_ "github.com/go-sql-driver/mysql"
	"k8s.io/klog"
)

const (
	dbDriver     = "mysql"
	mysqlTimeFmt = "2006-01-02 15:04:05.999999"
	dbName       = "root:delphin@tcp(delphin-mysql:3306)/delphin?timeout=5s"
	dbInitQuery  = `CREATE TABLE IF NOT EXISTS observation_logs
		(trial_name VARCHAR(255) NOT NULL,
		id INT AUTO_INCREMENT PRIMARY KEY,
		time DATETIME(6),
		metric_name VARCHAR(255) NOT NULL,
		value TEXT NOT NULL)`
	dbAddQuery = `INSERT INTO observation_logs (
				trial_name,
				time,
				metric_name,
				value
			) VALUES (?, ?, ?, ?)`
)

type dbConn struct {
	db *sql.DB
}

func NewDelphinDBInterface() (DelphinDBInterface, error) {

	ticker := time.NewTicker(5 * time.Second)
	defer ticker.Stop()

	timeoutC := time.After(60 * time.Second)
	for {
		select {

		// Tick time
		case <-ticker.C:
			if db, err := sql.Open(dbDriver, dbName); err == nil {
				d := new(dbConn)
				d.db = db
				return d, nil
			} else {
				klog.Errorf("DB failed: %v", err)
			}

		// DB connection Timeout
		case <-timeoutC:
			return nil, fmt.Errorf("DB connection Timeout")
		}
	}
}

func (d *dbConn) AddToDB(trialName string, observationLog *v1alpha1.ObservationLog) error {
	var mname, mvalue string
	for _, mlog := range observationLog.MetricLogs {
		mname = mlog.Metric.Name
		mvalue = mlog.Metric.Value
		if mlog.TimeStamp == "" {
			continue
		}
		t, err := time.Parse(time.RFC3339Nano, mlog.TimeStamp)
		if err != nil {
			return fmt.Errorf("Error parsing start time %s: %v", mlog.TimeStamp, err)
		}
		sqlTimeStr := t.UTC().Format(mysqlTimeFmt)
		_, err = d.db.Exec(
			dbAddQuery,
			trialName,
			sqlTimeStr,
			mname,
			mvalue,
		)
		if err != nil {
			return err
		}
	}
	return nil
}

func (d *dbConn) DeleteObservationLog(trialName string) error {
	_, err := d.db.Exec("DELETE FROM observation_logs WHERE trial_name = ?", trialName)
	return err
}

func (d *dbConn) GetObservationLog(trialName string, metricName string, startTime string, endTime string) (*v1alpha1.ObservationLog, error) {
	qfield := []interface{}{trialName}
	qstr := ""
	if metricName != "" {
		qstr += " AND metric_name = ?"
		qfield = append(qfield, metricName)
	}
	if startTime != "" {
		s_time, err := time.Parse(time.RFC3339Nano, startTime)
		if err != nil {
			return nil, fmt.Errorf("Error parsing start time %s: %v", startTime, err)
		}
		formattedStartTime := s_time.UTC().Format(mysqlTimeFmt)
		qstr += " AND time >= ?"
		qfield = append(qfield, formattedStartTime)
	}
	if endTime != "" {
		e_time, err := time.Parse(time.RFC3339Nano, endTime)
		if err != nil {
			return nil, fmt.Errorf("Error parsing completion time %s: %v", endTime, err)
		}
		formattedEndTime := e_time.UTC().Format(mysqlTimeFmt)
		qstr += " AND time <= ?"
		qfield = append(qfield, formattedEndTime)
	}
	rows, err := d.db.Query("SELECT time, metric_name, value FROM observation_logs WHERE trial_name = ?"+qstr+" ORDER BY time",
		qfield...)
	if err != nil {
		return nil, fmt.Errorf("Failed to get ObservationLogs %v", err)
	}
	result := &v1alpha1.ObservationLog{
		MetricLogs: []*v1alpha1.MetricLog{},
	}
	for rows.Next() {
		var mname, mvalue, sqlTimeStr string
		err := rows.Scan(&sqlTimeStr, &mname, &mvalue)
		if err != nil {
			klog.Errorf("Error scanning log: %v", err)
			continue
		}
		ptime, err := time.Parse(mysqlTimeFmt, sqlTimeStr)
		if err != nil {
			klog.Errorf("Error parsing time %s: %v", sqlTimeStr, err)
			continue
		}
		timeStamp := ptime.UTC().Format(time.RFC3339Nano)
		result.MetricLogs = append(result.MetricLogs, &v1alpha1.MetricLog{
			TimeStamp: timeStamp,
			Metric: &v1alpha1.Metric{
				Name:  mname,
				Value: mvalue,
			},
		})
	}
	return result, nil
}

func (d *dbConn) InitMySql() {
	db := d.db
	klog.Info("Initializing DB schema")
	_, err := db.Exec(dbInitQuery)
	if err != nil {
		klog.Fatalf("Error creating observation_logs table: %v", err)
	}
}
