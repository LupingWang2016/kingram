package db

import (
	v1alpha1 "delphin/api/v1alpha1/manager"
)

type DelphinDBInterface interface {
	InitMySql()
	AddToDB(trialName string, observationLog *v1alpha1.ObservationLog) error
	GetObservationLog(trialName string, metricName string, startTime string, endTime string) (*v1alpha1.ObservationLog, error)
	DeleteObservationLog(trialName string) error
}
