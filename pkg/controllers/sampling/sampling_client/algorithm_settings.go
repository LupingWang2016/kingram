package samplingclient

import (
	delphinv1alpha1 "delphin/api/v1alpha1"
	samplingapi "delphin/api/v1alpha1/manager"
)

// appendAlgorithmSettingsFromSampling appends the algorithm settings
// in sampling to Experiment.
// Algorithm settings in sampling will overwrite the settings in experiment.
func appendAlgorithmSettingsFromSampling(experiment *delphinv1alpha1.ProfilingExperiment, algoSettingsInSampling []delphinv1alpha1.AlgorithmSetting) {
	algoSettingsInExperiment := experiment.Spec.Algorithm
	for _, setting := range algoSettingsInSampling {
		if index, found := contains(
			algoSettingsInExperiment.AlgorithmSettings, setting.Name); found {
			// If the setting is found in Experiment, update it.
			algoSettingsInExperiment.AlgorithmSettings[index].Value = setting.Value
		} else {
			// If not found, append it.
			algoSettingsInExperiment.AlgorithmSettings = append(
				algoSettingsInExperiment.AlgorithmSettings, setting)
		}
	}
}

func updateAlgorithmSettings(sampling *delphinv1alpha1.Sampling, algorithm *samplingapi.AlgorithmSpec) {
	for _, setting := range algorithm.AlgorithmSetting {
		if setting != nil {
			if index, found := contains(sampling.Spec.Algorithm.AlgorithmSettings, setting.Name); found {
				// If the setting is found in Sampling, update it.
				sampling.Spec.Algorithm.AlgorithmSettings[index].Value = setting.Value
			} else {
				// If not found, append it.
				sampling.Spec.Algorithm.AlgorithmSettings = append(sampling.Spec.Algorithm.AlgorithmSettings, delphinv1alpha1.AlgorithmSetting{
					Name:  setting.Name,
					Value: setting.Value,
				})
			}
		}
	}
}

func contains(algorithmSettings []delphinv1alpha1.AlgorithmSetting,
	name string) (int, bool) {
	for i, s := range algorithmSettings {
		if s.Name == name {
			return i, true
		}
	}
	return -1, false
}
