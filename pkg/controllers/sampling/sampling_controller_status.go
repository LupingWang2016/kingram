package sampling

import (
	"context"
	delphinv1alpha1 "delphin/api/v1alpha1"
	"k8s.io/apimachinery/pkg/api/equality"
)

const (
	SamplingDeploymentReady    = "DeploymentReady"
	SamplingDeploymentNotReady = "DeploymentNotReady"
	SamplingRunningReason      = "SamplingRunning"
	SamplingFailedReason       = "SamplingFailed"
)

func (r *SamplingReconciler) updateStatus(s *delphinv1alpha1.Sampling, oldS *delphinv1alpha1.Sampling) error {
	if !equality.Semantic.DeepEqual(s.Status, oldS.Status) {
		if err := r.Update(context.TODO(), s); err != nil {
			return err
		}
	}
	return nil
}

func (r *SamplingReconciler) updateStatusCondition(s *delphinv1alpha1.Sampling, oldS *delphinv1alpha1.Sampling) error {
	if !equality.Semantic.DeepEqual(s.Status.Conditions, oldS.Status.Conditions) {
		newConditions := s.Status.Conditions
		s.Status = oldS.Status
		s.Status.Conditions = newConditions
		if err := r.Update(context.TODO(), s); err != nil {
			return err
		}
	}
	return nil
}
