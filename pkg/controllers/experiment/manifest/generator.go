package manifest

import (
	delphinv1alpha1 "delphin/api/v1alpha1"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"delphin/pkg/controllers/consts"
	delphinclient "delphin/pkg/util/delphinclient"
	"delphin/pkg/util/delphinconfig"
)

const (
	defaultMetricsCollectorTemplateName = "defaultMetricsCollectorTemplate.yaml"
)

// Generator is the type for manifests Generator.
type Generator interface {
	InjectClient(c client.Client)
	GetSamplingConfigData(algorithmName string) (map[string]string, error)
	GetMetricsCollectorImage(cKind delphinv1alpha1.CollectorKind) (string, error)
}

// DefaultGenerator is the default implementation of Generator.
type DefaultGenerator struct {
	client delphinclient.Client
}

// New creates a new Generator.
func New(c client.Client) Generator {
	delphinClient := delphinclient.NewWithGivenClient(c)
	return &DefaultGenerator{
		client: delphinClient,
	}
}

func (g *DefaultGenerator) InjectClient(c client.Client) {
	g.client.InjectClient(c)
}

func (g *DefaultGenerator) GetMetricsCollectorImage(cKind delphinv1alpha1.CollectorKind) (string, error) {
	configData, err := delphinconfig.GetMetricsCollectorConfigData(cKind, g.client.GetClient())
	if err != nil {
		return "", nil
	}
	return configData[consts.LabelMetricsCollectorSidecarImage], nil
}

func (g *DefaultGenerator) GetSamplingConfigData(algorithmName string) (map[string]string, error) {
	return delphinconfig.GetSamplingConfigData(algorithmName, g.client.GetClient(), "default")
}
