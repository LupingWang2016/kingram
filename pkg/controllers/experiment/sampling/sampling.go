package sampling

import (
	"context"
	//"delphin/pkg/controllers/experiment"

	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"

	//utilrand "k8s.io/apimachinery/pkg/util/rand"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"

	delphinv1alpha1 "delphin/api/v1alpha1"
)

type Sampling interface {
	GetOrCreateSampling(suggestionRequests int32, instance *delphinv1alpha1.ProfilingExperiment, samplingRequests *delphinv1alpha1.ObjectiveSpec) (*delphinv1alpha1.Sampling, error)
	UpdateSampling(sampling *delphinv1alpha1.Sampling) error
	UpdateSamplingStatus(sampling *delphinv1alpha1.Sampling) error
}

var log = logf.Log.WithName("experiment-suggestion-client")

type General struct {
	scheme *runtime.Scheme
	client.Client
}

func New(scheme *runtime.Scheme, client client.Client) Sampling {
	return &General{scheme: scheme, Client: client}
}

// GetOrCreateSampling get the  sampling instance
func (g *General) GetOrCreateSampling(suggestionRequests int32, instance *delphinv1alpha1.ProfilingExperiment, samplingRequests *delphinv1alpha1.ObjectiveSpec) (*delphinv1alpha1.Sampling, error) {
	logger := log.WithValues("experiment", types.NamespacedName{Name: instance.Name, Namespace: instance.Namespace})

	// Fetch sampling instance
	sampling := &delphinv1alpha1.Sampling{}
	err := g.Get(context.TODO(),
		types.NamespacedName{Name: instance.GetName(), Namespace: instance.GetNamespace()}, sampling)

	// Create a new sampling instance if there is no one
	if err != nil && errors.IsNotFound(err) {
		logger.Info("Creating Sampling", "namespace", instance.Namespace, "name", instance.Name, "requests", samplingRequests)
		if err := g.createSampling(instance, suggestionRequests); err != nil {
			logger.Error(err, "CreateSampling failed", "instance", instance.Name)
			return nil, err
		}
	} else if err != nil {
		logger.Error(err, "Sampling get failed", "instance", instance.Name)
		return nil, err
	} else {
		return sampling, nil
	}

	return nil, nil
}

// createSampling create a new sampling instance
func (g *General) createSampling(instance *delphinv1alpha1.ProfilingExperiment, suggestionRequests int32) error {
	logger := log.WithValues("experiment", types.NamespacedName{Name: instance.Name, Namespace: instance.Namespace})
	sampling := &delphinv1alpha1.Sampling{
		ObjectMeta: metav1.ObjectMeta{
			Name:        instance.Name,
			Namespace:   instance.Namespace,
			Labels:      instance.Labels,
			Annotations: instance.Annotations,
		},
		Spec: delphinv1alpha1.SamplingSpec{
			Algorithm:    instance.Spec.Algorithm,
			NumSamplings: suggestionRequests,
		},
	}

	if err := controllerutil.SetControllerReference(instance, sampling, g.scheme); err != nil {
		logger.Error(err, "Error in setting controller reference")
		return err
	}
	logger.Info("Creating sampling", "namespace", instance.Namespace, "name", instance.Name)
	if err := g.Create(context.TODO(), sampling); err != nil {
		return err
	}
	return nil
}

func (g *General) UpdateSampling(sampling *delphinv1alpha1.Sampling) error {
	if err := g.Update(context.TODO(), sampling); err != nil {
		return err
	}
	return nil
}

func (g *General) UpdateSamplingStatus(sampling *delphinv1alpha1.Sampling) error {
	if err := g.Update(context.TODO(), sampling); err != nil {
		return err
	}

	return nil
}
