package consts

import "delphin/pkg/util/env"

const (
	// ConfigExperimentSamplingName is the config name of the
	// sampling client implementation in experiment controller.
	ConfigExperimentSamplingName = "experiment-sampling-name"
	// ConfigCertLocalFS is the config name which indicates if we
	// should store the cert in file system.
	ConfigCertLocalFS = "cert-local-filesystem"
	// ConfigInjectSecurityContext is the config name which indicates
	// if we should inject the security context into the metrics collector
	// sidecar.
	ConfigInjectSecurityContext = "inject-security-context"
	// ConfigEnableGRPCProbeInSampling is the config name which indicates
	// if we should set GRPC probe in sampling deployments.
	ConfigEnableGRPCProbeInSampling = "enable-grpc-probe-in-sampling"

	// LabelExperimentName is the label of experiment name.
	LabelExperimentName = "experiment"
	// LabelSamplingName is the label of sampling name.
	LabelSamplingName = "sampling"
	// LabelDeploymentName is the label of deployment name.
	LabelDeploymentName = "deployment"

	// ContainerSampling is the container name in Sampling.
	ContainerSampling = "sampling"

	// DefaultSamplingPort is the default port of sampling service.
	DefaultServicePort = 8500
	// DefaultSamplingPortName is the default port name of sampling service.
	DefaultServicePortName = "delphin-service"

	// DefaultSamplingPort is the default port of sampling service.
	DefaultSamplingPort = 6789
	// DefaultSamplingPortName is the default port name of sampling service.
	DefaultSamplingPortName = "delphin-api"
	// DefaultGRPCService is the default service name in Sampling,
	// which is used to run healthz check using grpc probe.
	DefaultGRPCService = "manager.v1alpha3.Suggestion"

	// DefaultDelphinNamespaceEnvName is the default env name of delphin namespace
	DefaultDelphinNamespaceEnvName = "DELPHIN_CORE_NAMESPACE"
	// DefaultDelphinComposerEnvName is the default env name of delphin sampling composer
	DefaultDelphinComposerEnvName = "DELPHIN_SUGGESTION_COMPOSER"

	// DefaultDelphinDBManagerServiceNamespaceEnvName is the env name of Delphin DB Manager namespace
	DefaultDelphinDBManagerServiceNamespaceEnvName = "DELPHIN_DB_MANAGER_SERVICE_NAMESPACE"
	// DefaultDelphinDBManagerServiceIPEnvName is the env name of Delphin DB Manager IP
	DefaultDelphinDBManagerServiceIPEnvName = "DELPHIN_DB_MANAGER_SERVICE_IP"
	// DefaultDelphinDBManagerServicePortEnvName is the env name of Delphin DB Manager Port
	DefaultDelphinDBManagerServicePortEnvName = "DELPHIN_DB_MANAGER_SERVICE_PORT"

	// DelphinConfigMapName is the config map constants
	// Configmap name which includes Delphin's configuration
	DelphinConfigMapName = "delphin-config"
	// LabelSamplingTag is the name of sampling config in configmap.
	LabelSamplingTag = "sampling"
	// LabelSamplingImageTag is the name of sampling image config in configmap.
	LabelSamplingImageTag = "image"
	// LabelSamplingCPULimitTag is the name of sampling CPU Limit config in configmap.
	LabelSamplingCPULimitTag = "cpuLimit"
	// DefaultCPULimit is the default value for CPU Limit
	DefaultCPULimit = "500m"
	// LabelSamplingCPURequestTag is the name of sampling CPU Request config in configmap.
	LabelSamplingCPURequestTag = "cpuRequest"
	// DefaultCPURequest is the default value for CPU Request
	DefaultCPURequest = "50m"
	// LabelSamplingMemLimitTag is the name of sampling Mem Limit config in configmap.
	LabelSamplingMemLimitTag = "memLimit"
	// DefaultMemLimit is the default value for mem Limit
	DefaultMemLimit = "100Mi"
	// LabelSamplingMemRequestTag is the name of sampling Mem Request config in configmap.
	LabelSamplingMemRequestTag = "memRequest"
	// DefaultMemRequest is the default value for mem Request
	DefaultMemRequest = "10Mi"
	// LabelSamplingDiskLimitTag is the name of sampling Disk Limit config in configmap.
	LabelSamplingDiskLimitTag = "diskLimit"
	// DefaultDiskLimit is the default value for disk limit.
	DefaultDiskLimit = "5Gi"
	// LabelSamplingDiskRequestTag is the name of sampling Disk Request config in configmap.
	LabelSamplingDiskRequestTag = "diskRequest"
	// DefaultDiskRequest is the default value for disk request.
	DefaultDiskRequest = "500Mi"
	// LabelSamplingImagePullPolicy is the name of sampling image pull policy in configmap.
	LabelSamplingImagePullPolicy = "imagePullPolicy"
	// LabelSamplingServiceAccountName is the name of sampling service account in configmap.
	LabelSamplingServiceAccountName = "serviceAccountName"
	// DefaultImagePullPolicy is the default value for image pull policy.
	DefaultImagePullPolicy = "IfNotPresent"
	// LabelMetricsCollectorSidecar is the name of metrics collector config in configmap.
	LabelMetricsCollectorSidecar = "metrics-collector-sidecar"
	// LabelMetricsCollectorSidecarImage is the name of metrics collector image config in configmap.
	LabelMetricsCollectorSidecarImage = "image"
	// LabelMetricsCollectorCPULimitTag is the name of metrics collector CPU Limit config in configmap.
	LabelMetricsCollectorCPULimitTag = "cpuLimit"
	// LabelMetricsCollectorCPURequestTag is the name of metrics collector CPU Request config in configmap.
	LabelMetricsCollectorCPURequestTag = "cpuRequest"
	// LabelMetricsCollectorMemLimitTag is the name of metrics collector Mem Limit config in configmap.
	LabelMetricsCollectorMemLimitTag = "memLimit"
	// LabelMetricsCollectorMemRequestTag is the name of metrics collector Mem Request config in configmap.
	LabelMetricsCollectorMemRequestTag = "memRequest"
	// LabelMetricsCollectorDiskLimitTag is the name of metrics collector Disk Limit config in configmap.
	LabelMetricsCollectorDiskLimitTag = "diskLimit"
	// LabelMetricsCollectorDiskRequestTag is the name of metrics collector Disk Request config in configmap.
	LabelMetricsCollectorDiskRequestTag = "diskRequest"
	// LabelMetricsCollectorImagePullPolicy is the name of metrics collector image pull policy in configmap.
	LabelMetricsCollectorImagePullPolicy = "imagePullPolicy"

	// ReconcileErrorReason is the reason when there is a reconcile error.
	ReconcileErrorReason = "ReconcileError"

	// JobKindJob is the kind of the Kubernetes Job.
	JobKindJob = "Job"
	// JobKindTF is the kind of TFJob.
	JobKindTF = "TFJob"
	// JobKindPyTorch is the kind of PyTorchJob.
	JobKindPyTorch = "PyTorchJob"

	// built-in JobRoles
	JobRole        = "job-role"
	JobRoleTF      = "tf-job-role"
	JobRolePyTorch = "pytorch-job-role"

	// AnnotationIstioSidecarInjectName is the annotation of Istio Sidecar
	AnnotationIstioSidecarInjectName = "sidecar.istio.io/inject"

	// AnnotationIstioSidecarInjectValue is the value of Istio Sidecar annotation
	AnnotationIstioSidecarInjectValue = "false"

	// LabelTrialTemplateConfigMapName is the label name for the Trial templates configMap
	LabelTrialTemplateConfigMapName = "app"
	// LabelTrialTemplateConfigMapValue is the label value for the Trial templates configMap
	LabelTrialTemplateConfigMapValue = "delphin-trial-templates"

	ImageSamplingAlgorithmRandom = "gcr.io/kubeflow-images-public/DELPHIN/v1alpha3/suggestion-hyperopt"
)

//
var (
	// DefaultDelphinNamespace is the default namespace of delphin deployment.
	DefaultDelphinNamespace = env.GetEnvOrDefault(DefaultDelphinNamespaceEnvName, "asijob-pai")
	// DefaultComposer is the default composer of delphin sampling.
	// TODO: namespace
	DefaultComposer = env.GetEnvOrDefault(DefaultDelphinComposerEnvName, "General")

	// DefaultDelphinDBManagerServiceNamespace is the default namespace of Delphin DB Manager
	DefaultDelphinDBManagerServiceNamespace = env.GetEnvOrDefault(DefaultDelphinDBManagerServiceNamespaceEnvName, DefaultDelphinNamespace)
	// DefaultDelphinDBManagerServiceIP is the default IP of Delphin DB Manager
	DefaultDelphinDBManagerServiceIP = env.GetEnvOrDefault(DefaultDelphinDBManagerServiceIPEnvName, "delphin-db-manager")
	// DefaultDelphinDBManagerServicePort is the default Port of Delphin DB Manager
	DefaultDelphinDBManagerServicePort = env.GetEnvOrDefault(DefaultDelphinDBManagerServicePortEnvName, "6799")
)
