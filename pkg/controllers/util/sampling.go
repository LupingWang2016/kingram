package util

import (
	"fmt"

	delphinv1alpha1 "delphin/api/v1alpha1"
	"delphin/pkg/controllers/consts"
)

func GetAlgorithmDeploymentName(s *delphinv1alpha1.Sampling) string {
	// TODO: We comment the following parts, as we are using long-running algorithm server
	return s.Name + "-" + string(s.Spec.Algorithm.AlgorithmName) //s.Name + "-" +
}

func GetAlgorithmServiceName(s *delphinv1alpha1.Sampling) string {
	// TODO: We comment the following parts, as we are using long-running algorithm server
	return s.Name + "-" + string(s.Spec.Algorithm.AlgorithmName) //s.Name + "-" +
}

// GetAlgorithmEndpoint returns the endpoint of the algorithm service.
func GetAlgorithmEndpoint(s *delphinv1alpha1.Sampling) string {

	serviceName := GetAlgorithmServiceName(s) //"127.0.0.1" //
	return fmt.Sprintf("%s:%d",
		serviceName,
		//s.Namespace,
		consts.DefaultSamplingPort)
}
