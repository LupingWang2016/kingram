package util

import (
	"fmt"

	delphinv1alpha1 "delphin/api/v1alpha1"
	"delphin/pkg/controllers/consts"
)

func GetServiceDeploymentName(t *delphinv1alpha1.Trial) string {
	return t.Name + "-" + "deployment"
}

func GetServiceName(t *delphinv1alpha1.Trial) string {
	return t.Name + "-" + "service"
}

func GetServiceEndpoint(t *delphinv1alpha1.Trial) string {
	return fmt.Sprintf("%s:%d",
		GetServiceName(t),
		consts.DefaultServicePort)
}
