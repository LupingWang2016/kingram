package util

import (
	delphinv1alpha1 "delphin/api/v1alpha1"
	"delphin/pkg/controllers/consts"
)

// SamplingLabels returns the expected sampling labels.
func SamplingLabels(instance *delphinv1alpha1.Sampling) map[string]string {
	res := make(map[string]string)
	for k, v := range instance.Labels {
		res[k] = v
	}
	res[consts.LabelDeploymentName] = GetAlgorithmDeploymentName(instance)
	res[consts.LabelExperimentName] = instance.Name
	res[consts.LabelSamplingName] = instance.Name

	return res
}

// TrialLabels returns the expected trial labels.
func TrialLabels(instance *delphinv1alpha1.ProfilingExperiment) map[string]string {
	res := make(map[string]string)
	for k, v := range instance.Labels {
		res[k] = v
	}
	res[consts.LabelExperimentName] = instance.Name

	return res
}
