package managerclient

import (
	delphinv1alpha1 "delphin/api/v1alpha1"
	api_pb "delphin/api/v1alpha1/manager"
	common "delphin/pkg/db"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/client-go/kubernetes"
	typev1 "k8s.io/client-go/kubernetes/typed/core/v1"
	"k8s.io/client-go/tools/clientcmd"
	logs "log"
	logf "sigs.k8s.io/controller-runtime/pkg/log"

	"path/filepath"
)

// ManagerClient is the interface for delphin manager client in trial controller.
type ManagerClient interface {
	GetTrialObservationLog(
		instance *delphinv1alpha1.Trial, pod *corev1.Pod) (*api_pb.GetObservationLogReply, error)
	DeleteTrialObservationLog(
		instance *delphinv1alpha1.Trial) (*api_pb.DeleteObservationLogReply, error)
}

var (
	log = logf.Log.WithName("DefaultClient")
)

// DefaultClient implements the Client interface.
type DefaultClient struct {
}

// New creates a new ManagerClient.
func New() ManagerClient {

	return &DefaultClient{}
}

func (d *DefaultClient) GetTrialObservationLog(
	// read GetObservationLog call and update observation field

	instance *delphinv1alpha1.Trial, pod *corev1.Pod) (*api_pb.GetObservationLogReply, error) {
	if &instance.Spec.Objective == nil || &instance.Spec.Objective.ObjectiveMetricName == nil || &instance.Name == nil {
		return nil, nil
	}

	objectiveMetricName := instance.Spec.Objective.ObjectiveMetricName
	request := &api_pb.GetObservationLogRequest{
		TrialName:  instance.Name,
		MetricName: objectiveMetricName,
	}
	reply, err := common.GetObservationLog(request)
	return reply, err
}

func getClient(configLocation string) (typev1.CoreV1Interface, error) {
	kubeconfig := filepath.Clean(configLocation)
	config, err := clientcmd.BuildConfigFromFlags("", kubeconfig)
	if err != nil {
		logs.Fatal(err)
	}
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, err
	}
	return clientset.CoreV1(), nil
}

func (d *DefaultClient) DeleteTrialObservationLog(
	instance *delphinv1alpha1.Trial) (*api_pb.DeleteObservationLogReply, error) {
	request := &api_pb.DeleteObservationLogRequest{
		TrialName: instance.Name,
	}
	reply, err := common.DeleteObservationLog(request)
	if err != nil {
		return nil, err
	}
	return reply, nil
}
