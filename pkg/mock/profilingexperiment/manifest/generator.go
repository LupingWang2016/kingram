// Code generated by MockGen. DO NOT EDIT.
// Source: generator.go

// Package mock_manifest is a generated GoMock package.
package mock_manifest

import (
	v1alpha1 "delphin/api/v1alpha1"
	gomock "github.com/golang/mock/gomock"
	reflect "reflect"
	client "sigs.k8s.io/controller-runtime/pkg/client"
)

// MockGenerator is a mock of Generator interface
type MockGenerator struct {
	ctrl     *gomock.Controller
	recorder *MockGeneratorMockRecorder
}

// MockGeneratorMockRecorder is the mock recorder for MockGenerator
type MockGeneratorMockRecorder struct {
	mock *MockGenerator
}

// NewMockGenerator creates a new mock instance
func NewMockGenerator(ctrl *gomock.Controller) *MockGenerator {
	mock := &MockGenerator{ctrl: ctrl}
	mock.recorder = &MockGeneratorMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use
func (m *MockGenerator) EXPECT() *MockGeneratorMockRecorder {
	return m.recorder
}

// InjectClient mocks base method
func (m *MockGenerator) InjectClient(c client.Client) {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "InjectClient", c)
}

// InjectClient indicates an expected call of InjectClient
func (mr *MockGeneratorMockRecorder) InjectClient(c interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "InjectClient", reflect.TypeOf((*MockGenerator)(nil).InjectClient), c)
}

// GetSamplingConfigData mocks base method
func (m *MockGenerator) GetSamplingConfigData(algorithmName string) (map[string]string, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetSamplingConfigData", algorithmName)
	ret0, _ := ret[0].(map[string]string)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetSamplingConfigData indicates an expected call of GetSamplingConfigData
func (mr *MockGeneratorMockRecorder) GetSamplingConfigData(algorithmName interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetSamplingConfigData", reflect.TypeOf((*MockGenerator)(nil).GetSamplingConfigData), algorithmName)
}

// GetMetricsCollectorImage mocks base method
func (m *MockGenerator) GetMetricsCollectorImage(cKind v1alpha1.CollectorKind) (string, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "GetMetricsCollectorImage", cKind)
	ret0, _ := ret[0].(string)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// GetMetricsCollectorImage indicates an expected call of GetMetricsCollectorImage
func (mr *MockGeneratorMockRecorder) GetMetricsCollectorImage(cKind interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "GetMetricsCollectorImage", reflect.TypeOf((*MockGenerator)(nil).GetMetricsCollectorImage), cKind)
}
