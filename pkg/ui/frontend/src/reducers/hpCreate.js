import * as actions from '../actions/hpCreateActions';
import * as constants from '../constants/constants';

const initialState = {
    loading: false,
    commonParametersMetadata: [
        {
            name: 'name',
            value: 'random-experiment',
            description: 'A name of an experiment',
        },
        {
            name: 'namespace',
            value: 'asijob-pai',
            description: 'Namespace to deploy an experiment',
        },
    ],
    commonParametersSpec: [
        {
            name: 'maxNumTrials',
            value: '3',
            description: 'How many trials can be processed in parallel',
        },
        {
            name: 'requestTemplate',
            value: '{"imageUrls":["http:\/\/gw3.alicdn.com\/tfscom\/O1CN01CYg8v61RaD3QWUwYa_!!385132127-0-lubanu-s.jpg"],"bizId":100720,"requestId":"0","context":{"passthrough":"","class":"com.taobao.tmap.request.TmapContext"},"id":"0","source":"test","class":"com.alibaba.tmap.algo.content.ImageRequest"}',
            description: 'How many trials can be processed in parallel',
        },
        {
            name: 'serviceName',
            value: 'demoService',
            description: 'How many trials can be processed in parallel',
        },
    ],
    allObjectiveTypes: ['minimize', 'maximize'],
    objective: [
        {
            name: 'type',
            value: 'maximize',
            description: 'Type of optimization',
        },
        {
            name: 'objectiveMetricName',
            value: 'qps',
            description: 'Name for the objective metric',
        },
    ],
    algorithmName: 'random',
    allAlgorithms: ['grid', 'random', 'bayesianoptimization'],
    algorithmSettings: [],
    parameters: [
        {
            name: 'cpu',
            parameterType: 'double',
            feasibleSpace: 'feasibleSpace',
            min: '0.01',
            max: '0.03',
            list: [],
        },

    ],
    allParameterTypes: ['int', 'double'],
    currentYaml: '',
    mcCustomContainerYaml: '',
    servicecurrentYaml: 'template:\n' +
        '  metadata:\n' +
        '    annotations:\n' +
        '      mvap/revision: "1606452442985"\n' +
        '      pod.beta1.sigma.ali/alarming-off-upgrade: "true"\n' +
        '      pod.beta1.sigma.ali/alloc-spec: \'{"containers": [{ "name": "102182","resource":{"cpu": {"cpuSet": {"spreadStrategy": "spread","cpuIDs": []}}}}]}\'\n' +
        '      pod.beta1.sigma.ali/disable-lifecycle-hook-inject: "true"\n' +
        '      pod.beta1.sigma.ali/naming-register-state: working_online\n' +
        '    labels:\n' +
        '      ali/metric-bizId: "100720"\n' +
        '      ali/metric-versionId: "102182"\n' +
        '      mvap.pod.resopuce.cpu/102182: "4"\n' +
        '      mvap.pod.resopuce.gpu/102182: "0"\n' +
        '      mvap.pod.resopuce.gpumem/102182: "0"\n' +
        '      mvap.pod.resopuce.memory/102182: "16"\n' +
        '      mvap/environment: prepub\n' +
        '      mvap/model: "102182"\n' +
        '      mvap/node-type: cpu\n' +
        '      mvap/resource-group: prepub\n' +
        '      sigma.ali/app-name: mvap-resources-gpu\n' +
        '      sigma.ali/instance-group: mvap-resources-gpuhost-docker\n' +
        '      sigma.ali/resource-pool: mvap\n' +
        '      sigma.alibaba-inc.com/app-stage: PRE_PUBLISH\n' +
        '      sigma.alibaba-inc.com/app-unit: CENTER_UNIT.center\n' +
        '    namespace: asijob-pai\n' +
        '  spec:\n' +
        '    affinity:\n' +
        '      nodeAffinity:\n' +
        '        requiredDuringSchedulingIgnoredDuringExecution:\n' +
        '          nodeSelectorTerms:\n' +
        '            - matchExpressions:\n' +
        '                - key: sigma.ali/node-sn\n' +
        '                  operator: In\n' +
        '                  values:\n' +
        '                    - "i-8vbgt047thtuwi05jt5c"\n' +
        '    containers:\n' +
        '      - command:\n' +
        '        - sudo sh\n' +
        '        - -c \'/home/admin/start.sh --tfs_batch_timeout_micros=50000 --tfs_model_name=102182\n' +
        '          --project_name=mvap --hsf_http_port_range=12220,12220 --tfs_enable_batching=null\n' +
        '          --flowcontrol_diamond_group=flowcontrol_diamond_group --hsf_port_range=12200,12200\n' +
        '          --oss_access_key=LTAIZ14u7TN4tCK5 --env_name=prepub --flowcontrol_diamond_dataid=flowcontrol_diamond_dataid\n' +
        '          --hsf_group=102182-prepub --user_defined_package=com.alibaba.tmap,tmap-framework-algo,4.1.5-SNAPSHOT\n' +
        '          --tfs_max_enqueued_batches=10000000 --tfs_num_batch_threads=8 --gpu_fraction=0\n' +
        '          --oss_access_secret=HXGbCyX1xzTs0j7pRwyNe5UrdNKLI9 --tfs_max_batch_size=8\n' +
        '          --oss_endpoint=cn-shanghai.oss.aliyun-inc.com --resources=oss://mvap-modeldb-online/model/100147/20201126221556/static/\n' +
        '          --version_id=102182 --tfs_port_range=12222,12222 --pandora_qos_port_range=12221,12221\n' +
        '          --container_type=cpu --biz_id=100720 >> /home/admin/console.log 2>&1\'\n' +
        '        env:\n' +
        '        - name: K8S_POD_NAME\n' +
        '          valueFrom:\n' +
        '            fieldRef:\n' +
        '              apiVersion: v1\n' +
        '              fieldPath: metadata.name\n' +
        '        - name: SN\n' +
        '          valueFrom:\n' +
        '            fieldRef:\n' +
        '              apiVersion: v1\n' +
        '              fieldPath: metadata.name\n' +
        '        - name: kmonitor_env_version_id\n' +
        '          value: "102182"\n' +
        '        - name: ali_run_mode\n' +
        '          value: common_vm\n' +
        '        - name: SCHEULDER_VERSION\n' +
        '          value: "20190219"\n' +
        '        - name: SCRIPT_VERSION\n' +
        '          value: master\n' +
        '        - name: kmonitor_env_biz_id\n' +
        '          value: "100720"\n' +
        '        image: reg.docker.alibaba-inc.com/mvap/serving_base_rpc:5.0\n' +
        '        imagePullPolicy: IfNotPresent\n' +
        '        lifecycle:\n' +
        '          postStart:\n' +
        '            exec:\n' +
        '              command:\n' +
        '                - /home/admin/pod_start.sh\n' +
        '          preStop:\n' +
        '            exec:\n' +
        '              command:\n' +
        '                - /home/admin/stop.sh\n' +
        '        livenessProbe:\n' +
        '          exec:\n' +
        '            command:\n' +
        '              - /home/admin/check_status.sh\n' +
        '          failureThreshold: 3\n' +
        '          initialDelaySeconds: 30\n' +
        '          periodSeconds: 10\n' +
        '          successThreshold: 1\n' +
        '          timeoutSeconds: 1\n' +
        '        name: "102182"\n' +
        '        readinessProbe:\n' +
        '          exec:\n' +
        '            command:\n' +
        '              - /home/admin/health.sh\n' +
        '          failureThreshold: 3\n' +
        '          initialDelaySeconds: 30\n' +
        '          periodSeconds: 10\n' +
        '          successThreshold: 1\n' +
        '          timeoutSeconds: 1\n' +
        '        resources:\n' +
        '          limits:\n' +
        '            cpu: "4"\n' +
        '            memory: 16G\n' +
        '            nvidia.com/gpu: "0"\n' +
        '            nvidia.com/gpumem: "0"\n' +
        '            sigma/eni: "1"\n' +
        '          requests:\n' +
        '            cpu: "4"\n' +
        '            memory: 16G\n' +
        '            nvidia.com/gpu: "0"\n' +
        '            nvidia.com/gpumem: "0"\n' +
        '            sigma/eni: "1"\n' +
        '        terminationMessagePath: /dev/termination-log\n' +
        '        terminationMessagePolicy: File\n' +
        '        volumeMounts:\n' +
        '        - mountPath: /share_data/99c944b3f0\n' +
        '          name: mediadata-99c944b3f0\n' +
        '        - mountPath: /share_data/979ce4a169\n' +
        '          name: mediadata-979ce4a169\n' +
        '        - mountPath: /share_data/99f5a4afac\n' +
        '          name: mediadata-99f5a4afac\n' +
        '        - mountPath: /share_data/9d5114ba5b\n' +
        '          name: mediadata-9d5114ba5b\n' +
        '        - mountPath: /share_data/931644a178\n' +
        '          name: mediadata-931644a178\n' +
        '        - mountPath: /data_001\n' +
        '          name: data\n' +
        '    dnsPolicy: ClusterFirst\n' +
        '    restartPolicy: Always\n' +
        '    schedulerName: default-scheduler\n' +
        '    securityContext: { }\n' +
        '    terminationGracePeriodSeconds: 60\n' +
        '    tolerations:\n' +
        '    - effect: NoSchedule\n' +
        '      key: sigma.ali/resource-pool\n' +
        '      operator: Equal\n' +
        '      value: mvap\n' +
        '    - effect: NoSchedule\n' +
        '      key: sigma.ali/is-ecs\n' +
        '      operator: Equal\n' +
        '      value: "true"\n' +
        '    - effect: NoSchedule\n' +
        '      key: sigma.ali/resource-pool\n' +
        '      operator: Equal\n' +
        '      value: sigma_public\n' +
        '    - effect: NoSchedule\n' +
        '      key: sigma.ali/is-ecs\n' +
        '      operator: Exists\n' +
        '    - effect: NoSchedule\n' +
        '      key: sigma.ali/eni-limit-exceeded\n' +
        '      operator: Exists\n' +
        '    - effect: NoExecute\n' +
        '      key: node.kubernetes.io/not-ready\n' +
        '      operator: Exists\n' +
        '      tolerationSeconds: 300\n' +
        '    - effect: NoExecute\n' +
        '      key: node.kubernetes.io/unreachable\n' +
        '      operator: Exists\n' +
        '      tolerationSeconds: 300\n' +
        '    - effect: NoSchedule\n' +
        '      key: alibabacloud.com/partition\n' +
        '      operator: Equal\n' +
        '      value: gpu\n' +
        '    - effect: NoSchedule\n' +
        '      key: ali.SubBizPool\n' +
        '      operator: Equal\n' +
        '      value: ailabs\n' +
        '    volumes:\n' +
        '    - name: mediadata-99c944b3f0\n' +
        '      persistentVolumeClaim:\n' +
        '        claimName: mvap-pvc-99c944b3f0\n' +
        '    - name: mediadata-979ce4a169\n' +
        '      persistentVolumeClaim:\n' +
        '        claimName: mvap-pvc-979ce4a169\n' +
        '    - name: mediadata-99f5a4afac\n' +
        '      persistentVolumeClaim:\n' +
        '        claimName: mvap-pvc-99f5a4afac\n' +
        '    - name: mediadata-9d5114ba5b\n' +
        '      persistentVolumeClaim:\n' +
        '        claimName: mvap-pvc-9d5114ba5b\n' +
        '    - name: mediadata-931644a178\n' +
        '      persistentVolumeClaim:\n' +
        '        claimName: mvap-pvc-931644a178\n' +
        '    - hostPath:\n' +
        '        path: /home/t4/sharedata/\n' +
        '        type: ""\n' +
        '      name: data',
    clientcurrentYaml: 'metadata:\n' +
        '  labels:\n' +
        '    sigma.ali/site: na61\n' +
        '    alibabacloud.com/qos: LS\n' +
        '    creat: manual\n' +
        '    env: prepub\n' +
        '    pod.beta1.sigma.ali/upgrading-state: Succeeded\n' +
        '    sigma.ali/app-name: mvap-resources-gpu\n' +
        '    sigma.ali/instance-group: mvap-resources-gpuhost-docker\n' +
        '    sigma.alibaba-inc.com/app-stage: PRE_PUBLISH\n' +
        '    sigma.alibaba-inc.com/app-unit: CENTER_UNIT.center\n' +
        '  namespace: "asijob-pai"\n' +
        'spec:\n' +
        '  template:\n' +
        '    metadata:\n' +
        '      labels:\n' +
        '        sigma.ali/site: na61\n' +
        '        alibabacloud.com/qos: LS\n' +
        '        creat: manual\n' +
        '        env: prepub\n' +
        '        pod.beta1.sigma.ali/upgrading-state: Succeeded\n' +
        '        sigma.ali/app-name: mvap-resources-gpu\n' +
        '        sigma.ali/instance-group: mvap-resources-gpuhost-docker\n' +
        '        sigma.alibaba-inc.com/app-stage: PRE_PUBLISH\n' +
        '        sigma.alibaba-inc.com/app-unit: CENTER_UNIT.center\n' +
        '    spec:\n' +
        '      imagePullSecrets:\n' +
        '      - name: harborsecretkey\n' +
        '      affinity:\n' +
        '        nodeAffinity:\n' +
        '          requiredDuringSchedulingIgnoredDuringExecution:\n' +
        '            nodeSelectorTerms:\n' +
        '              - matchExpressions:\n' +
        '                  - key: sigma.ali/node-sn\n' +
        '                    operator: In\n' +
        '                    values:\n' +
        '                      - "i-8vbgt047thtuwi05jt5c"\n' +
        '      tolerations:\n' +
        '      - effect: NoSchedule\n' +
        '        key: sigma.ali/resource-pool\n' +
        '        operator: Equal\n' +
        '        value: sigma_public\n' +
        '      - effect: NoSchedule\n' +
        '        key: sigma.ali/resource-pool\n' +
        '        operator: Equal\n' +
        '        value: ackee_pool\n' +
        '      - effect: NoSchedule\n' +
        '        key: sigma.ali/is-ecs\n' +
        '        operator: Exists\n' +
        '      - effect: NoSchedule\n' +
        '        key: sigma.ali/eni-limit-exceeded\n' +
        '        operator: Exists\n' +
        '      - effect: NoExecute\n' +
        '        key: node.kubernetes.io/not-ready\n' +
        '        operator: Exists\n' +
        '        tolerationSeconds: 300\n' +
        '      - effect: NoExecute\n' +
        '        key: node.kubernetes.io/unreachable\n' +
        '        operator: Exists\n' +
        '        tolerationSeconds: 300\n' +
        '      - effect: NoSchedule\n' +
        '        key: alibabacloud.com/partition\n' +
        '        operator: Equal\n' +
        '        value: gpu\n' +
        '      - effect: NoSchedule\n' +
        '        key: ali.SubBizPool\n' +
        '        operator: Equal\n' +
        '        value: ailabs\n' +
        '      - effect: NoSchedule\n' +
        '        key: sigma.ali/resource-pool\n' +
        '        value: mit_bach\n' +
        '      - effect: NoSchedule\n' +
        '        key: sigma.ali/is-ecs\n' +
        '        operator: Exists\n' +
        '      - effect: NoExecute\n' +
        '        key: node.kubernetes.io/not-ready\n' +
        '        operator: Exists\n' +
        '        tolerationSeconds: 300\n' +
        '      - effect: NoExecute\n' +
        '        key: node.kubernetes.io/unreachable\n' +
        '        operator: Exists\n' +
        '        tolerationSeconds: 300\n' +
        '\n' +
        '      containers:\n' +
        '      - name: pi\n' +
        '        resources:\n' +
        '          limits:\n' +
        '            cpu: "1"\n' +
        '            sigma/eni: "1"\n' +
        '          requests:\n' +
        '            sigma/eni: "1"\n' +
        '            cpu: "1"\n' +
        '        image: registry.cn-hangzhou.aliyuncs.com/delphin/delphin-client:43\n' +
        '        env:\n' +
        '        - name: serviceVersion\n' +
        '          value: "100720_102182_prepub"\n' +
        '        - name: func\n' +
        '          value: "processImage"\n' +
        '        - name: Inferface\n' +
        '          value: "com.alibaba.tmap.algo.ContentAlgoRemoteService"\n' +
        '\n' +
        '        command: [ "python3" ]\n' +
        '        args: ["/workspace/hsf_client_mvap/profiling_delphin.py"]\n' +
        '        imagePullPolicy: IfNotPresent\n' +
        '      restartPolicy: Never\n' +
        '  backoffLimit: 4',
};

const filterValue = (obj, key) => {
    return obj.findIndex(p => p.name === key);
};

const hpCreateReducer = (state = initialState, action) => {
    switch (action.type) {
        case actions.CHANGE_YAML_HP:
            return {
                ...state,
                currentYaml: action.payload,
            };
        case actions.CHANGE_YAML_HP_SERVICE:
            return {
                ...state,
                servicecurrentYaml: action.payload,
            };
        case actions.CHANGE_YAML_HP_CLIENT:
            return {
                ...state,
                clientcurrentYaml: action.payload,
            };
        case actions.CHANGE_META_HP:
            let meta = state.commonParametersMetadata.slice();
            let index = filterValue(meta, action.name);
            meta[index].value = action.value;
            return {
                ...state,
                commonParametersMetadata: meta,
            };
        case actions.CHANGE_SPEC_HP:
            let spec = state.commonParametersSpec.slice();
            index = filterValue(spec, action.name);
            spec[index].value = action.value;
            return {
                ...state,
                commonParametersSpec: spec,
            };
        case actions.CHANGE_OBJECTIVE_HP:
            let obj = state.objective.slice();
            index = filterValue(obj, action.name);
            obj[index].value = action.value;
            return {
                ...state,
                objective: obj,
            };
        case actions.ADD_METRICS_HP:
            let additionalMetricNames = state.additionalMetricNames.slice();
            additionalMetricNames.push({
                value: '',
            });
            return {
                ...state,
                additionalMetricNames: additionalMetricNames,
            };
        case actions.DELETE_METRICS_HP:
            additionalMetricNames = state.additionalMetricNames.slice();
            additionalMetricNames.splice(action.index, 1);
            return {
                ...state,
                additionalMetricNames: additionalMetricNames,
            };
        case actions.EDIT_METRICS_HP:
            additionalMetricNames = state.additionalMetricNames.slice();
            additionalMetricNames[action.index].value = action.value;
            return {
                ...state,
                additionalMetricNames: additionalMetricNames,
            };
        case actions.CHANGE_ALGORITHM_NAME_HP:
            return {
                ...state,
                algorithmName: action.algorithmName,
            };
        case actions.ADD_ALGORITHM_SETTING_HP:
            let algorithmSettings = state.algorithmSettings.slice();
            let setting = { name: '', value: '' };
            algorithmSettings.push(setting);
            return {
                ...state,
                algorithmSettings: algorithmSettings,
            };
        case actions.CHANGE_ALGORITHM_SETTING_HP:
            algorithmSettings = state.algorithmSettings.slice();
            algorithmSettings[action.index][action.field] = action.value;
            return {
                ...state,
                algorithmSettings: algorithmSettings,
            };
        case actions.DELETE_ALGORITHM_SETTING_HP:
            algorithmSettings = state.algorithmSettings.slice();
            algorithmSettings.splice(action.index, 1);
            return {
                ...state,
                algorithmSettings: algorithmSettings,
            };
        case actions.ADD_PARAMETER_HP:
            let parameters = state.parameters.slice();
            parameters.push({
                name: '',
                parameterType: '',
                feasibleSpace: 'feasibleSpace',
                min: '',
                max: '',
                list: [],
            });
            return {
                ...state,
                parameters: parameters,
            };
        case actions.EDIT_PARAMETER_HP:
            parameters = state.parameters.slice();
            parameters[action.index][action.field] = action.value;
            return {
                ...state,
                parameters: parameters,
            };
        case actions.DELETE_PARAMETER_HP:
            parameters = state.parameters.slice();
            parameters.splice(action.index, 1);
            return {
                ...state,
                parameters: parameters,
            };
        case actions.ADD_LIST_PARAMETER_HP:
            parameters = state.parameters.slice();
            parameters[action.paramIndex].list.push({
                value: '',
            });
            return {
                ...state,
                parameters: parameters,
            };
        case actions.EDIT_LIST_PARAMETER_HP:
            parameters = state.parameters.slice();
            parameters[action.paramIndex].list[action.index].value = action.value;
            return {
                ...state,
                parameters: parameters,
            };
        case actions.DELETE_LIST_PARAMETER_HP:
            parameters = state.parameters.slice();
            parameters[action.paramIndex].list.splice(action.index, 1);
            return {
                ...state,
                parameters: parameters,
            };
        // Metrics Collector Kind change
        case actions.CHANGE_MC_KIND_HP:
            let newMCSpec = JSON.parse(JSON.stringify(state.mcSpec));
            newMCSpec.collector.kind = action.kind;

            if (
                action.kind === constants.MC_KIND_FILE ||
                action.kind === constants.MC_KIND_TENSORFLOW_EVENT ||
                action.kind === constants.MC_KIND_CUSTOM
            ) {
                let newKind;
                switch (action.kind) {
                    case constants.MC_KIND_FILE:
                        newKind = constants.MC_FILE_SYSTEM_KIND_FILE;
                        break;

                    case constants.MC_KIND_TENSORFLOW_EVENT:
                        newKind = constants.MC_FILE_SYSTEM_KIND_DIRECTORY;
                        break;

                    default:
                        newKind = constants.MC_FILE_SYSTEM_NO_KIND;
                }
                // File or TF Event Kind
                newMCSpec.source.fileSystemPath = {
                    kind: newKind,
                    path: '',
                };
            } else if (action.kind === constants.MC_KIND_PROMETHEUS) {
                // Prometheus Kind
                newMCSpec.source.httpGet = {
                    port: constants.MC_PROMETHEUS_DEFAULT_PORT,
                    path: constants.MC_PROMETHEUS_DEFAULT_PATH,
                    scheme: constants.MC_HTTP_GET_HTTP_SCHEME,
                    host: '',
                    httpHeaders: [],
                };
            }

            return {
                ...state,
                mcSpec: newMCSpec,
                mcCustomContainerYaml: '',
            };
        // File System Path change
        case actions.CHANGE_MC_FILE_SYSTEM_HP:
            newMCSpec = JSON.parse(JSON.stringify(state.mcSpec));
            newMCSpec.source.fileSystemPath.kind = action.kind;
            newMCSpec.source.fileSystemPath.path = action.path;
            return {
                ...state,
                mcSpec: newMCSpec,
            };
        // HTTPGet settings
        case actions.CHANGE_MC_HTTP_GET_HP:
            newMCSpec = JSON.parse(JSON.stringify(state.mcSpec));

            newMCSpec.source.httpGet.port = action.port;
            newMCSpec.source.httpGet.path = action.path;
            newMCSpec.source.httpGet.scheme = action.scheme;
            newMCSpec.source.httpGet.host = action.host;

            return {
                ...state,
                mcSpec: newMCSpec,
            };
        // Collector HTTPGet Headers
        case actions.ADD_MC_HTTP_GET_HEADER_HP:
            newMCSpec = JSON.parse(JSON.stringify(state.mcSpec));
            let currentHeaders = newMCSpec.source.httpGet.httpHeaders.slice();
            let newHeader = { name: '', value: '' };
            currentHeaders.push(newHeader);
            newMCSpec.source.httpGet.httpHeaders = currentHeaders;
            return {
                ...state,
                mcSpec: newMCSpec,
            };
        case actions.CHANGE_MC_HTTP_GET_HEADER_HP:
            newMCSpec = JSON.parse(JSON.stringify(state.mcSpec));
            currentHeaders = newMCSpec.source.httpGet.httpHeaders.slice();
            currentHeaders[action.index][action.fieldName] = action.value;
            newMCSpec.source.httpGet.httpHeaders = currentHeaders;
            return {
                ...state,
                mcSpec: newMCSpec,
            };
        case actions.DELETE_MC_HTTP_GET_HEADER_HP:
            newMCSpec = JSON.parse(JSON.stringify(state.mcSpec));
            currentHeaders = newMCSpec.source.httpGet.httpHeaders.slice();
            currentHeaders.splice(action.index, 1);
            newMCSpec.source.httpGet.httpHeaders = currentHeaders;
            return {
                ...state,
                mcSpec: newMCSpec,
            };
        // Collector Custom container
        case actions.CHANGE_MC_CUSTOM_CONTAINER_HP:
            return {
                ...state,
                mcCustomContainerYaml: action.yamlContainer,
            };
        // Collector Metrics Format
        case actions.ADD_MC_METRICS_FORMAT_HP:
            newMCSpec = JSON.parse(JSON.stringify(state.mcSpec));
            let currentFormats = newMCSpec.source.filter.metricsFormat.slice();
            currentFormats.push('');
            newMCSpec.source.filter.metricsFormat = currentFormats;
            return {
                ...state,
                mcSpec: newMCSpec,
            };
        case actions.CHANGE_MC_METRIC_FORMAT_HP:
            newMCSpec = JSON.parse(JSON.stringify(state.mcSpec));
            currentFormats = newMCSpec.source.filter.metricsFormat.slice();
            currentFormats[action.index] = action.format;
            newMCSpec.source.filter.metricsFormat = currentFormats;
            return {
                ...state,
                mcSpec: newMCSpec,
            };
        case actions.DELETE_MC_METRIC_FORMAT_HP:
            newMCSpec = JSON.parse(JSON.stringify(state.mcSpec));
            currentFormats = newMCSpec.source.filter.metricsFormat.slice();
            currentFormats.splice(action.index, 1);
            newMCSpec.source.filter.metricsFormat = currentFormats;
            return {
                ...state,
                mcSpec: newMCSpec,
            };
        default:
            return state;
    }
};

export default hpCreateReducer;
