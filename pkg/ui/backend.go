package ui

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/ghodss/yaml"
	"google.golang.org/grpc"
	"sigs.k8s.io/controller-runtime/pkg/client"

	delphinv1alpha1 "delphin/api/v1alpha1"
	api_pb_v1alpha1 "delphin/api/v1alpha1/manager"
	common_v1alpha1 "delphin/pkg/db"
	delphinclient "delphin/pkg/util/delphinclient"
	"k8s.io/api/batch/v1beta1"
	v1 "k8s.io/api/core/v1"
)

func NewDelphinUIHandler() *DelphinUIHandler {
	kclient, err := delphinclient.NewClient(client.Options{})
	if err != nil {
		log.Printf("NewClient for Delphin failed: %v", err)
		panic(err)
	}
	return &DelphinUIHandler{
		delphinClient: kclient,
	}
}

func (k *DelphinUIHandler) connectManager() (*grpc.ClientConn, api_pb_v1alpha1.ManagerClient) {
	conn, err := grpc.Dial(common_v1alpha1.GetDBManagerAddr(), grpc.WithInsecure())
	if err != nil {
		log.Printf("Dial to GRPC failed: %v", err)
		return nil, nil
	}
	c := api_pb_v1alpha1.NewManagerClient(conn)
	return conn, c
}

func (k *DelphinUIHandler) SubmitProfilingYamlJob(w http.ResponseWriter, r *http.Request) {
	//enableCors(&w)
	var data map[string]interface{}

	json.NewDecoder(r.Body).Decode(&data)

	job := delphinv1alpha1.ProfilingExperiment{}
	if yamlContent, ok := data["yaml"].(string); ok {
		err := yaml.Unmarshal([]byte(yamlContent), &job)
		if err != nil {
			log.Printf("Unmarshal YAML content failed: %v", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		err = k.delphinClient.CreateExperiment(&job)
		if err != nil {
			log.Printf("Create Profiling from YAML failed: %v", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}

}

func (k *DelphinUIHandler) SubmitProfilingParametersJob(w http.ResponseWriter, r *http.Request) {
	var data map[string]map[string]interface{}

	json.NewDecoder(r.Body).Decode(&data)

	job := delphinv1alpha1.ProfilingExperiment{}

	if yamlContent, ok := data["yaml"]["raw"]; ok {
		jsonbody, err := json.Marshal(yamlContent)
		if err != nil {
			log.Printf("Marshal data for HP job failed: %v", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		if err := json.Unmarshal(jsonbody, &job); err != nil {
			log.Printf("Unmarshal HP job failed: %v", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}

	json.NewDecoder(r.Body).Decode(&data)

	servicePodTemplate := v1.PodTemplate{}
	if yamlContent, ok := data["yaml"]["servicePodTemplate"].(string); ok {
		err := yaml.Unmarshal([]byte(yamlContent), &servicePodTemplate)
		if err != nil {
			log.Printf("Unmarshal YAML content failed: %v", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
	serviceClientTemplate := v1beta1.JobTemplateSpec{}
	if yamlContent, ok := data["yaml"]["serviceClientTemplate"].(string); ok {
		err := yaml.Unmarshal([]byte(yamlContent), &serviceClientTemplate)
		if err != nil {
			log.Printf("Unmarshal YAML content failed: %v", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
	serviceClientTemplate.DeepCopyInto(&job.Spec.ClientJobTemplate)
	servicePodTemplate.DeepCopyInto(&job.Spec.ServicePodTemplate)
	err := k.delphinClient.CreateExperiment(&job)
	if err != nil {
		log.Printf("Profiling from YAML failed: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}

func (k *DelphinUIHandler) SubmitTrialYamlJob(w http.ResponseWriter, r *http.Request) {
	//enableCors(&w)
	var data map[string]interface{}

	json.NewDecoder(r.Body).Decode(&data)

	job := delphinv1alpha1.Trial{}
	if yamlContent, ok := data["yaml"].(string); ok {
		err := yaml.Unmarshal([]byte(yamlContent), &job)
		if err != nil {
			log.Printf("Unmarshal YAML content failed: %v", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		err = k.delphinClient.CreateTrial(&job)
		if err != nil {
			log.Printf("Create Trial from YAML failed: %v", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
}

func (k *DelphinUIHandler) DeleteExperiment(w http.ResponseWriter, r *http.Request) {
	experimentName := r.URL.Query()["experimentName"][0]
	namespace := r.URL.Query()["namespace"][0]

	experiment, err := k.delphinClient.GetExperiment(experimentName, namespace)
	if err != nil {
		log.Printf("GetExperiment failed: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	err = k.delphinClient.DeleteExperiment(experiment)
	if err != nil {
		log.Printf("DeleteExperiment failed: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (k *DelphinUIHandler) FetchNamespaces(w http.ResponseWriter, r *http.Request) {

	// Get all available namespaces
	namespaces, err := k.getAvailableNamespaces()
	if err != nil {
		log.Printf("GetAvailableNamespaces failed: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	response, err := json.Marshal(namespaces)
	if err != nil {
		log.Printf("Marshal namespaces failed: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(response)
}

// FetchExperiment gets experiment in specific namespace.
func (k *DelphinUIHandler) FetchExperiment(w http.ResponseWriter, r *http.Request) {
	experimentName := r.URL.Query()["experimentName"][0]
	namespace := r.URL.Query()["namespace"][0]

	experiment, err := k.delphinClient.GetExperiment(experimentName, namespace)
	if err != nil {
		log.Printf("GetExperiment failed: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	response, err := json.Marshal(experiment)
	if err != nil {
		log.Printf("Marshal Experiment failed: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(response)
}

// FetchSuggestion gets suggestion in specific namespace
func (k *DelphinUIHandler) FetchSuggestion(w http.ResponseWriter, r *http.Request) {
	suggestionName := r.URL.Query()["suggestionName"][0]
	namespace := r.URL.Query()["namespace"][0]

	suggestion, err := k.delphinClient.GetSampling(suggestionName, namespace)
	if err != nil {
		log.Printf("GetSuggestion failed: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	response, err := json.Marshal(suggestion)
	if err != nil {
		log.Printf("Marshal Suggestion failed: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(response)
}
