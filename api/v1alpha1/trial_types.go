/*
Copyright 2020 The Alibaba Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	"k8s.io/api/batch/v1beta1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// TrialSpec defines the pressure test task under a specific configuration
type TrialSpec struct {
	SamplingResult     []ParameterAssignment   `json:"samplingResult,omitempty"`        // Sampling results, including all the parameter values to be tuned
	ServicePodTemplate v1.PodTemplate          `json:"servicePodTemplate,omitempty"`    // Yaml file to start the service pod
	RequestTemplate    string                  `json:"requestTemplate,omitempty"`       // Pressure test request, json format
	Constraint         ConstraintSepc          `json:"constraintSepc,omitempty"`        // Describes the constraints during pressure testing, e.g., RT < 2.5 seconds
	ServiceName        string                  `json:"serviceName,omitempty"`           // Unique service id, e.g., "com.alibaba.tmap.algo.AlgoRemoteService:100026_101517_prepub"
	ClientJobTemplate  v1beta1.JobTemplateSpec `json:"serviceClientTemplate,omitempty"` // Yaml file to start the service Job
	Objective          ObjectiveSpec           `json:"objective,omitempty"`             // Describes the objective of the experiment.
}

// TrialStatus defines the status of this pressure test
type TrialStatus struct {
	TrialResult    *TrialResult     `json:"trialResult,omitempty"` // Including the TrialAssignment and the Objective value (e.g., QPS)
	Conditions     []TrialCondition `json:"conditions,omitempty"`  // Observed runtime condition for this Trial.
	StartTime      *metav1.Time     `json:"startTime,omitempty"`
	CompletionTime *metav1.Time     `json:"completionTime,omitempty"`
}

type TrialCondition struct {
	// Type of trial condition.
	Type TrialConditionType `json:"type"`

	// Standard Kubernetes object's LastTransitionTime
	LastTransitionTime metav1.Time `json:"lastTransitionTime,omitempty"`

	// Status of the condition, one of True, False, Unknown.
	Status v1.ConditionStatus `json:"status"`

	// A human readable message indicating details about the transition.
	Message string `json:"message,omitempty"`

	// The last time this condition was updated.
	LastUpdateTime metav1.Time `json:"lastUpdateTime,omitempty"`
}

type TrialConditionType string

const (
	TrialRunning   TrialConditionType = "Running"
	TrialSucceeded TrialConditionType = "Succeeded"
	TrialFailed    TrialConditionType = "Failed"
	TrialCreated   TrialConditionType = "Created"
	TrialPending   TrialConditionType = "Pending"
	TrialKilled    TrialConditionType = "Killed"
)

// +kubebuilder:object:root=true
// +kubebuilder:printcolumn:name="State",type=string,JSONPath=`.status.conditions[-1:].type`
// +kubebuilder:printcolumn:name="Age",type=date,JSONPath=`.metadata.creationTimestamp`
// +kubebuilder:printcolumn:name="Object Name",type=string,JSONPath=`.status.trialResult.objectiveMetrics[-1:].name`
// +kubebuilder:printcolumn:name="Object Value",type=string,JSONPath=`.status.trialResult.objectiveMetrics[-1:].value`
// +kubebuilder:printcolumn:name="Parameters",type=string,JSONPath=`.spec.samplingResult`

// Trial is the Schema for the trials API
type Trial struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   TrialSpec   `json:"spec,omitempty"`
	Status TrialStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// TrialList contains a list of Trial
type TrialList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Trial `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Trial{}, &TrialList{})
}
