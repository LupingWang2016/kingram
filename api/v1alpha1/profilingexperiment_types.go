/*
Copyright 2020 The Alibaba Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	"k8s.io/api/batch/v1beta1"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// ProfilingExperimentSpec defines the desired state of ProfilingExperiment
type ProfilingExperimentSpec struct {
	ServiceName        string                  `json:"serviceName,omitempty"`           // Unique service id, e.g., "com.alibaba.tmap.algo.AlgoRemoteService:100026_101517_prepub"
	TunableParameters  []ParameterCategory     `json:"tunableParameters,omitempty"`     // List of hyperparameter to be tuned.
	Objective          ObjectiveSpec           `json:"objective,omitempty"`             // Describes the objective of the experiment.
	Constraint         ConstraintSepc          `json:"constraintSepc,omitempty"`        // Describes the constraints during pressure testing, e.g., RT < 2.5 seconds
	Algorithm          AlgorithmSpec           `json:"algorithm,omitempty"`             // Sampling algorithm, e.g., Bayesian opt.
	MaxNumTrials       *int32                  `json:"maxNumTrials,omitempty"`          // Maximum sampling numbers
	RequestTemplate    string                  `json:"requestTemplate,omitempty"`       // Pressure test request, json format
	ServicePodTemplate v1.PodTemplate          `json:"servicePodTemplate,omitempty"`    // Yaml file to start the service pod
	ClientJobTemplate  v1beta1.JobTemplateSpec `json:"serviceClientTemplate,omitempty"` // Yaml file to start the service Job
}

type ProfilingExperimentStatus struct {
	Conditions          []ProfilingCondition `json:"conditions,omitempty"`          // List of observed runtime conditions for this ProfilingExperiment.
	CurrentOptimalTrial TrialResult          `json:"currentOptimalTrial,omitempty"` // Current optimal parameters
	TrialResultList     []TrialResult        `json:"trialResultList,omitempty"`     // Sampled configurations and the corresponding object values
	CompletionTime      *metav1.Time         `json:"completionTime,omitempty"`
	StartTime           *metav1.Time         `json:"startTime,omitempty"`

	// List of trial names which are running.
	RunningTrialList []string `json:"runningTrialList,omitempty"`

	// List of trial names which are pending.
	PendingTrialList []string `json:"pendingTrialList,omitempty"`

	// List of trial names which have already failed.
	FailedTrialList []string `json:"failedTrialList,omitempty"`

	// List of trial names which have already succeeded.
	SucceededTrialList []string `json:"succeededTrialList,omitempty"`

	// List of trial names which have been killed.
	KilledTrialList []string `json:"killedTrialList,omitempty"`

	// Trials is the total number of trials owned by the experiment.
	Trials int32 `json:"trials,omitempty"`

	// How many trials have succeeded.
	TrialsSucceeded int32 `json:"trialsSucceeded,omitempty"`

	// How many trials have been killed.
	TrialsKilled int32 `json:"trialsKilled,omitempty"`

	// How many trials are currently pending.
	TrialsPending int32 `json:"trialsPending,omitempty"`

	// How many trials have failed.
	TrialsRunning int32 `json:"trialsRunning,omitempty"`

	// How many trials have failed.
	TrialsFailed int32 `json:"trialsFailed,omitempty"`
}

type CollectorKind string

// +k8s:deepcopy-gen=true
// ProfilingExperimentCondition describes the state of the experiment at a certain point.
type ProfilingCondition struct {
	// Type of experiment condition.
	Type ProfilingConditionType `json:"type"`

	// Status of the condition, one of True, False, Unknown.
	Status v1.ConditionStatus `json:"status"`

	// A human readable message indicating details about the transition.
	Message string `json:"message,omitempty"`

	// The last time this condition was updated.
	LastUpdateTime metav1.Time `json:"lastUpdateTime,omitempty"`
}

type TrialResult struct {
	TunableParameters []ParameterAssignment `json:"tunableParameters,omitempty"` // Current parameter assignment of the trial
	ObjectiveMetrics  []Metric              `json:"objectiveMetrics,omitempty"`  // Object value under these parameters, e.g., QPS=100
}

type Metric struct {
	Name  string `json:"name,omitempty"`
	Value string    `json:"value,omitempty"`
}

// Types of the hyper-parameters to be tuned, we support the following several kinds of parameters.
type ParameterType string

const (
	ParameterTypeDouble      ParameterType = "double"
	ParameterTypeInt         ParameterType = "int"
	ParameterTypeDiscrete    ParameterType = "discrete"
	ParameterTypeCategorical ParameterType = "categorical"
)

// Categories of parameters, high-level parameter divisions, including env, args, resource
type ParameterCategory struct {
	Category   Category        `json:"category,omitempty"`
	Parameters []ParameterSpec `json:"parameters,omitempty"`
}

// Range of the hyper-parameters to be tuned
type FeasibleSpace struct {
	Max  string   `json:"max,omitempty"`  // max value
	Min  string   `json:"min,omitempty"`  // min value
	List []string `json:"list,omitempty"` // list of possible value
	Step string   `json:"step,omitempty"` // step of sampling
}

// Category of the hyper-parameters to be tuned, for patching use.
type Category string

const (
	CategoryResource Category = "resource"
	CategoryEnv      Category = "env"
	CategoryArgs     Category = "args"
)

// Meta data of a hyper-parameter to be tuned
type ParameterSpec struct {
	Name          string        `json:"name,omitempty"`
	ParameterType ParameterType `json:"parameterType,omitempty"`
	FeasibleSpace FeasibleSpace `json:"feasibleSpace,omitempty"`
}

// Optimization obj classes: minimize or maximize
type ObjectiveType string

const (
	ObjectiveTypeMinimize ObjectiveType = "minimize"
	ObjectiveTypeMaximize ObjectiveType = "maximize"
)

// Opt. constraint, e.g., RT should be controlled within 2.5 seconds
type ConstraintSepc struct {
	ConstraintName         string `json:"constraintName,omitempty"` // e.g., RT should be controlled under a threshold
	ConstraintThresholdMin string `json:"constraintThresholdMin,omitempty"`
	ConstraintThresholdMax string `json:"constraintThresholdMax,omitempty"` // 2.5 seconds

}

// Optimization obj, e.g., minimize the resource cost per QPS
type ObjectiveSpec struct {
	Type                ObjectiveType `json:"type,omitempty"`                // minimize or maximize
	ObjectiveMetricName string        `json:"objectiveMetricName,omitempty"` // e.g., GPUMemConsumptionPerQPS
}

// Parameters key-value pair of the Opt. algorithm
type AlgorithmSetting struct {
	Name  string `json:"name,omitempty"`
	Value string `json:"value,omitempty"`
}

// Supported searching algorithms
type AlgorithmName string

const (
	BayesianOpt  AlgorithmName = "BayesianOpt"
	RandomSearch AlgorithmName = "RandomSearch"
	GridSearch   AlgorithmName = "GridSearch"
)

// Specification of the Opt. algorithm
type AlgorithmSpec struct {
	AlgorithmName     AlgorithmName      `json:"algorithmName,omitempty"`
	AlgorithmSettings []AlgorithmSetting `json:"algorithmSettings,omitempty"` // Key-value pairs representing settings for sampling algorithms.
}

// Current hyper-parameter value (key-value pair)
type ParameterAssignment struct {
	Name     string   `json:"name,omitempty"`
	Value    string   `json:"value,omitempty"`
	Category Category `json:"category,omitempty"`
}

// status of the ProfilingExperiment
type ProfilingConditionType string

const (
	ProfilingCreated    ProfilingConditionType = "Created"
	ProfilingRunning    ProfilingConditionType = "Running"
	ProfilingRestarting ProfilingConditionType = "Restarting"
	ProfilingSucceeded  ProfilingConditionType = "Succeeded"
	ProfilingFailed     ProfilingConditionType = "Failed"
	ProfilingCompleted  ProfilingConditionType = "Completed"
)

// +kubebuilder:object:root=true
// +kubebuilder:printcolumn:name="State",type=string,JSONPath=`.status.conditions[-1:].type`
// +kubebuilder:printcolumn:name="Age",type=date,JSONPath=`.metadata.creationTimestamp`
// ProfilingExperiment is the Schema for the profilingexperiments API
type ProfilingExperiment struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   ProfilingExperimentSpec   `json:"spec,omitempty"`
	Status ProfilingExperimentStatus `json:"status,omitempty"`
}

// +k8s:deepcopy-gen=true
type SourceSpec struct {
	HttpGet        *v1.HTTPGetAction `json:"httpGet,omitempty"`
	FileSystemPath string            `json:"fileSystemPath,omitempty"`
}

// ProfilingExperimentList contains a list of ProfilingExperiment
// +k8s:deepcopy-gen:interfaces=k8s.io/apimachinery/pkg/runtime.Object
type ProfilingExperimentList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []ProfilingExperiment `json:"items"`
}

func init() {
	SchemeBuilder.Register(&ProfilingExperiment{}, &ProfilingExperimentList{})
}
