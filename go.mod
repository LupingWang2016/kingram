module delphin

go 1.15

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	github.com/awalterschulze/gographviz v2.0.3+incompatible
	github.com/ghodss/yaml v1.0.0
	github.com/go-logr/logr v0.1.0
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang/mock v1.4.4
	github.com/golang/protobuf v1.4.3
	github.com/grpc-ecosystem/grpc-gateway v1.9.5 // indirect
	github.com/kubeflow/pytorch-operator v0.6.0 // indirect
	github.com/kubeflow/tf-operator v0.5.3 // indirect
	github.com/onsi/ginkgo v1.11.0
	github.com/onsi/gomega v1.8.1
	github.com/prometheus/client_golang v1.0.0
	github.com/spf13/viper v1.3.2
	golang.org/x/net v0.0.0-20201010224723-4f7140c49acb
	golang.org/x/sys v0.0.0-20201015000850-e3ed0017c211 // indirect
	google.golang.org/genproto v0.0.0-20201015140912-32ed001d685c
	google.golang.org/grpc v1.32.0
	google.golang.org/protobuf v1.25.0 // indirect
	k8s.io/api v0.17.2
	k8s.io/apimachinery v0.17.2
	k8s.io/client-go v0.17.2
	k8s.io/klog v1.0.0
	sigs.k8s.io/controller-runtime v0.5.0

)

//replace k8s.io/api => k8s.io/api v0.18.6
//replace (
//	k8s.io/code-generator => k8s.io/code-generator v0.18.6
//	sigs.k8s.io/controller-tools/cmd/controller-gen => sigs.k8s.io/controller-tools/cmd/controller-gen v0.3.0
//)
