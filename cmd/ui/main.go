package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"

	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"

	"delphin/pkg/ui"
)

var (
	port, host, buildDir *string
)

func init() {
	port = flag.String("port", "80", "the port to listen to for incoming HTTP connections")
	host = flag.String("host", "0.0.0.0", "the host to listen to for incoming HTTP connections")
	buildDir = flag.String("build-dir", "/app/build", "the dir of frontend")
}
func main() {
	flag.Parse()
	kuh := ui.NewDelphinUIHandler()

	log.Printf("Serving the frontend dir %s", *buildDir)
	frontend := http.FileServer(http.Dir(*buildDir))
	http.Handle("/delphin/", http.StripPrefix("/delphin/", frontend))

	http.HandleFunc("/delphin/fetch_hp_jobs/", kuh.FetchAllHPJobs)
	http.HandleFunc("/delphin/submit_profiling_yaml/", kuh.SubmitProfilingYamlJob)
	http.HandleFunc("/delphin/submit_trial_yaml/", kuh.SubmitTrialYamlJob)

	http.HandleFunc("/delphin/delete_experiment/", kuh.DeleteExperiment)
	http.HandleFunc("/delphin/fetch_experiment/", kuh.FetchExperiment)
	http.HandleFunc("/delphin/fetch_suggestion/", kuh.FetchSuggestion)

	http.HandleFunc("/delphin/fetch_hp_job_info/", kuh.FetchHPJobInfo)
	http.HandleFunc("/delphin/fetch_hp_job_trial_info/", kuh.FetchHPJobTrialInfo)
	//http.HandleFunc("/delphin/fetch_nas_job_info/", kuh.FetchNASJobInfo)

	http.HandleFunc("/delphin/submit_hp_job/", kuh.SubmitProfilingParametersJob)

	//http.HandleFunc("/delphin/fetch_trial_templates/", kuh.FetchTrialTemplates)
	//http.HandleFunc("/delphin/add_template/", kuh.AddTemplate)
	//http.HandleFunc("/delphin/edit_template/", kuh.EditTemplate)
	//http.HandleFunc("/delphin/delete_template/", kuh.DeleteTemplate)
	http.HandleFunc("/delphin/fetch_namespaces", kuh.FetchNamespaces)

	log.Printf("Serving at %s:%s", *host, *port)
	if err := http.ListenAndServe(fmt.Sprintf("%s:%s", *host, *port), nil); err != nil {
		panic(err)
	}
}
